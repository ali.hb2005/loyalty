﻿using loyalty.Data;
using loyalty.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace loyalty.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MembersController : ControllerBase
    {
        private LoyaltyContext Context { get; }

        private readonly ILogger<MembersController> _logger;

        public MembersController(ILogger<MembersController> logger, LoyaltyContext context)
        {
            _logger = logger;
            Context = context;
        }

        [HttpGet]
        public IEnumerable<Object> Get()
        {
            return Context.Members.Select(m => new { m.Name, m.Address, Accounts = m.Accounts.Select(a => new { a.Account.Name, a.Balance, Status = a.IsActive ? Status.ACTIVE : Status.INACTIVE }) }).ToArray();
        }

        [HttpGet("GetMembersByBalance")]
        public IEnumerable<Object> GetMembersByBalance(Status status, int minBalance, int maxBalance)
        {
            return Context.Members.Select(m => new { Member = m, balance = m.Accounts.Where(a => a.IsActive == (status == Status.ACTIVE)).Sum(a => a.Balance) })
                .Where(s => (minBalance == 0 || s.balance >= minBalance) && (maxBalance == 0 || s.balance <= maxBalance))
                .Select(s => s.Member).Select(m => new { m.Name, m.Address, Accounts = m.Accounts.Select(a => new { a.Account.Name, a.Balance, Status = a.IsActive ? Status.ACTIVE : Status.INACTIVE }) }).ToArray();
        }

        [HttpPut]
        public Boolean Put(string name, string address)
        {
            string loweredName = name.ToLowerInvariant();
            if (Context.Members.SingleOrDefault(m => m.Name == loweredName) == null)
            {
                Context.Members.Add(new Member() { Name = name, Address = address });
                Context.SaveChanges();
                return true;
            }
            return false;
        }

        [HttpPost]
        public void Post(MemberIO[] members)
        {
            Dictionary<string, Account> accounts = members.SelectMany(m => m.Accounts).GroupBy(a => a.Name.ToLowerInvariant()).Select(g => new Account() { Name = g.First().Name }).ToDictionary(a => a.Name.ToLowerInvariant());
            Context.Accounts.AddRange(accounts.Values);
            foreach (var m in members)
            {
                Context.Members.Add(new Member() { Name = m.Name, Address = m.Address, Accounts = m.Accounts.Select(a => new Subscription() { Balance = a.Balance, IsActive = a.Status == Status.ACTIVE, Account = accounts[a.Name.ToLowerInvariant()] }).ToArray() });
            }
            Context.SaveChanges();
        }

        [HttpPost("UpdateAcccount")]
        public void UpdateAcccount(string memberName, string accountName, int balance, Status status)
        {
            string loweredMemName = memberName.ToLowerInvariant();
            Subscription subscription = null;
            Member member = Context.Members.Single(m => m.Name == loweredMemName);
            string loweredAccName = accountName.ToLowerInvariant();
            Account account = Context.Accounts.SingleOrDefault(m => m.Name == loweredAccName);
            if (account == null) Context.Accounts.Add(account);
            else
            {
                subscription = Context.Subscriptions.SingleOrDefault(s => s.Account.Name == account.Name && s.Member.Name == member.Name);
            }
            if (subscription == null)
            {
                subscription = new Subscription() { Account = account, Member = member };
                Context.Subscriptions.Add(subscription);
            }
            subscription.Balance += balance;
            subscription.IsActive = status == Status.ACTIVE;
            Context.SaveChanges();


        }
    }
}

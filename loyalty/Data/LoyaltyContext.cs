﻿using loyalty.Models;
using Microsoft.EntityFrameworkCore;
namespace loyalty.Data
{

    public class LoyaltyContext : DbContext
    {
        public LoyaltyContext(DbContextOptions<LoyaltyContext> options) : base(options)
        {
        }

        public DbSet<Member> Members { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Account> Accounts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Subscription>()
                .HasKey(s => new { s.AccountID, s.MemberID });

            modelBuilder.Entity<Subscription>()
                .HasOne(s => s.Member)
                .WithMany(m => m.Accounts)
                .HasForeignKey(s => s.MemberID);

            modelBuilder.Entity<Subscription>()
                .HasOne(s => s.Account)
                .WithMany(a => a.Members)
                .HasForeignKey(s => s.AccountID);

        }
    }
}

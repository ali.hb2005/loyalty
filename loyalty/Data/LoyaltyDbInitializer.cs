﻿using loyalty.Models;
using System.Linq;

namespace loyalty.Data
{
    public class LoyaltyDbInitializer
    {

        public static void Initialize(LoyaltyContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Members.Any())
            {
                return;   // DB has been seeded
            }

            context.Members.AddRange(new Member[]
            {
                new Member{Name="Carson",Address="Sonnemannstr. 67", Accounts = new Subscription[]{ new Subscription { Balance = 100, IsActive = true, AccountID = 1, MemberID = 1,Account = new Account { Name = "Netflix" } } } },
            });

            context.SaveChanges();

            //var accounts = new Account[] { new Account { Name = "Netflix" }, };
            //foreach (Account a in accounts)
            //{
            //    context.Accounts.Add(a);
            //}
            //context.SaveChanges();


            //var subs = new Subscription[] { new Subscription { Balance = 100, IsActive = true, AccountID = 1, MemberID = 1 }, };
            //foreach (Subscription s in subs)
            //{
            //    context.Subscriptions.Add(s);
            //}
            //context.SaveChanges();
        }

    }
}

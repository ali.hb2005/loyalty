﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace loyalty.Models
{
    public class Account
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Subscription> Members { get; set; }
    }
}

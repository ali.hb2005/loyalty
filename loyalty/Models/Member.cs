﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace loyalty.Models
{
    public class Member
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public ICollection<Subscription> Accounts { get; set; }
    }
}

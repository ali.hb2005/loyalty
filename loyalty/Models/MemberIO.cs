﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace loyalty.Models
{
    public class MemberIO
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public ICollection<AccountIO> Accounts { get; set; }
    }

    public class AccountIO
    {
        public string Name { get; set; }
        public int Balance { get; set; }
        public Status Status { get; set; }
    }
}

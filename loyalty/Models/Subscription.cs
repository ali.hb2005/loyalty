﻿using System.ComponentModel.DataAnnotations.Schema;

namespace loyalty.Models
{
    public class Subscription
    {
        public int MemberID { get; set; }
        public int AccountID { get; set; }
        public bool IsActive { get; set; }
        public int Balance { get; set; }

        public Member Member { get; set; }
        public Account Account { get; set; }
    }
}
